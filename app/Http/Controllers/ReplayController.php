<?php namespace App\Http\Controllers;

use App\Exceptions\{ReplayDuplicationException, ReplayException};
use App\Models\Replay;
use App\Services\ReplayParser;
use Illuminate\Http\Request;
use Storage;

class ReplayController extends Controller
{
	public function create()
	{
		return view('pages.replay.create');
	}

	public function store(Request $request, ReplayParser $parser)
	{
		$replayFile = $request->file('replay');
		
		try {
			$replay = $parser->storeReplay($replayFile->get());
		} catch (ReplayDuplicationException $e) {
			return response($e->getMessage(), 409);
		} catch (ReplayException $e) {
			return response($e->getMessage(), 422);
		}
		
		return redirect(action('ReplayController@show', compact('replay')));
	}
	
	public function show(Replay $replay)
	{
		return view('pages.replay.show', compact('replay'));
	}
	
	public function download(Replay $replay)
	{
		return Storage::download($replay->getFilepath(), "{$replay->map->map_tag}-guest.lmp");
	}
}

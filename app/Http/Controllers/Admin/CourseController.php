<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\{Course, Map};
use Illuminate\Http\Request;

class CourseController extends Controller
{
	public function index()
	{
		return view('pages.admin.course.index', ['courses' => Course::all()]);
	}
	
	public function create()
	{
		return $this->show(new Course());
	}
	
	public function store(Request $request)
	{
		return $this->update($request, new Course());
	}
	
	public function show(Course $course)
	{
		return view('pages.admin.course.show', ['course' => $course, 'freeMaps' => Map::doesntHave('course')->get()]);
	}
	
	public function update(Request $request, Course $course)
	{
		$course->fill($request->only('name'))->save();
		
		if (!empty($addMapId = $request->get('add_map'))) {
			Map::whereKey($addMapId)->update(['course_id' => $course->getKey()]);
		}
		
		if (!empty($removeMapIds = $request->get('remove_map'))) {
			Map::whereIn('id', $removeMapIds)->update(['course_id' => null]);
		}
		
		return redirect(action('Admin\CourseController@show', compact('course')));
	}
	
	public function destroy(Course $course)
	{
		$course->delete();
		
		return redirect(action('Admin\CourseController@index'));
	}
}

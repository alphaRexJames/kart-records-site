<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * A course that replays compete for position on.
 *
 * @property string   $name    The name of the course.
 * @property Map[]    $maps    The maps that encompass this course.
 * @property Replay[] $replays All replays submitted for this course.
 */
class Course extends Model
{
	protected $no_skips;
	
	protected $fillable = ['name'];
	
	public function maps()
	{
		return $this->hasMany(Map::class);
	}
	
	public function replays()
	{
		return $this->hasManyThrough(Replay::class, Map::class);
	}
	
	public function getTimeLeaderboardAttribute()
	{
		return $this->leaderboard('race_tics');
	}
	
	public function getLapLeaderboardAttribute()
	{
		return $this->leaderboard('lap_tics');
	}
	
	protected function leaderboard($column)
	{
		$allReplays = $this->replays()
			->where(function ($query) {
				if ($this->no_skips) {
					$query->where('replays.status', Replay::STATUS_VERIFIED);
				} else {
					$query->whereIn('replays.status', Replay::LEADERBOARD_STATUSES);
				}
				
				$query->orWhereNull('replays.status');
			})->orderBy("replays.$column", 'asc')->get();
		
		$names = [];
		
		$replays = $allReplays->filter(function (Replay $replay) use (&$names) {
			if (in_array($replay->name, $names)) {
				return false;
			}
			
			$names[] = $replay->name;
			
			return true;
		});
		
		return $replays;
	}
	
	public function hideMajorSkips()
	{
		$this->no_skips = true;
		
		return $this;
	}
	
	public function scopeWithReplayCount($query, Carbon $since = null)
	{
		$query->select('courses.*', \DB::raw('COUNT(replays.id) as replay_count'))
			->leftJoin('maps', 'maps.course_id', '=', 'courses.id')
			->leftJoin('replays', 'replays.map_id', '=', 'maps.id')
			->groupBy('courses.id');
		
		if ($since) {
			$query->where('replays.created_at', '>', $since);
		}
	}
}

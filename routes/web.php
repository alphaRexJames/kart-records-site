<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::resource('/replay', 'ReplayController', ['only' => ['create', 'store', 'show']]);
Route::get('/replay/{replay}/download', 'ReplayController@download');

Route::resource('/course', 'CourseController', ['only' => ['index', 'show']]);

Route::group(['prefix' => '/admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
	Route::get('/', function () {
		return view('pages.admin.index');
	});
	
	Route::resource('/course', 'CourseController');
	Route::resource('/replay', 'ReplayController');
});


Route::get('login', [
  'as' => 'login',
  'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
  'as' => '',
  'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
  'as' => 'logout',
  'uses' => 'Auth\LoginController@logout'
]);

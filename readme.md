# SRB2Kart records site

Here be code. I want the todo list to be public, so see `resources/views/pages/index.blade.php` for that.

## Setup

This project is built on Laravel 5.7. See their requirements for running this on your system. You should only need an
SQL database, dependency-wise.

```bash
git clone git@git.magicalgirl.moe:fickleheart/kart-records-site.git
cd kart-records-site
composer install
# Edit .env to set required variables, including database connection
php artisan migrate

# Make an admin user or seven
php artisan tinker
> App\Models\User::create(['username' => 'adminuser', 'password' => Hash::make('adminpassword')])
> exit
# Please use a different password than that
```

PHP 7.1 _should_ be fine. I'm writing this against 7.2 though, so maybe use that instead.

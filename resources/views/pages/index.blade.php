@extends('layouts.front')

@section('content')
<h1>SRB2Kart records</h1>

<p>Official(ish) time attack leaderboard for SRB2Kart.</p>

<h2>Recent replays</h2>
<table>
	<thead>
		<tr>
			<th>Course</th>
			<th>Name</th>
			<th>Time</th>
			<th>Character</th>
			<th>Verification</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach($recent_replays as $replay)
			<tr>
				<td>
					<a href="{{ action('CourseController@show', ['course' => $replay->course]) }}">
						{{ $replay->course->name }}
					</a>
				</td>
				<td>{{ $replay->name }}</td>
				<td>{{ $replay->race_time }}</td>
				<td>{{ $replay->character->name ?: $replay->character->skin }}</td>
				<td>{{ $replay->status }}</td>
				<td><a href="{{ action('ReplayController@show', compact('replay')) }}">Info</a></td>
				<td><a href="{{ action('ReplayController@download', compact('replay')) }}">Download</a></td>
			</tr>
		@endforeach
	</tbody>
</table>

<h2>Popular courses (last week)</h2>
<table>
	<thead>
		<tr>
			<th>Course</th>
			<th>Submissions</th>
		</tr>
	</thead>
	<tbody>
		@foreach($popular_courses as $course)
			<tr>
				<td>
					<a href="{{ action('CourseController@show', ['course' => $course]) }}">
						{{ $course->name }}
					</a>
				</td>
				<td>{{ $course->replay_count }}</td>
			</tr>
		@endforeach
	</tbody>
</table>

<h2>Replay Viewer</h2>
<p>
	To make "Watch" links functional, for Windows users, download
	<a href="/watch_srb2kart_replay.bat">the replay viewer script</a>
	and place it somewhere convenient, like your SRB2Kart folder. Open it and change the variables at the top to
	configure it, then when your browser asks how to open srb2kartreplay: links, point it to the script. (You may have
	to navigate to its folder and enter the filename manually if your browser restricts the search to EXE files only.)
</p>
<p>This script has been tested in Firefox on Windows 10 and may need modifications to work with other browsers.</p>

<h2>Replays Verified By</h2>
<ul>
	@foreach($admins as $admin)
		<li>{{ $admin->username }}</li>
	@endforeach
</ul>
@stop

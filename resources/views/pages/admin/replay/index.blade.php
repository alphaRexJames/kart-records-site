@extends('layouts.admin')

@section('content')
<h1>Replays</h1>

<a href="{{ action('Admin\ReplayController@index', ['unchecked' => true]) }}">Unchecked Only</a>

<table>
	<thead>
		<tr>
			<th>Name</th>
			<th>Course</th>
			<th>Time</th>
			<th>Lap</th>
			<th>Status</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach($replays as $replay)
			<tr>
				<td>{{ $replay->name }}</td>
				<td>{{ $replay->course ? $replay->course->name : $replay->map->map_tag }}</td>
				<td>{{ $replay->race_time }}</td>
				<td>{{ $replay->lap_time }}</td>
				<td>
					<form action="{{ action('Admin\ReplayController@update', compact('replay')) }}" method="POST">
						@csrf
						<input type="hidden" name="_method" value="PUT" />
					
						<select name="status">
							<option value="" {{ $replay->status ? 'disabled' : 'selected' }}>NOT CHECKED</option>
							@foreach($statuses as $status)
								<option value="{{ $status }}" {{ $replay->status == $status ? 'selected' : '' }}>{{ $status }}</option>
							@endforeach
						</select>
						
						<input type="submit" value="Update" />
					</form>
				</td>
				<td><a href="{{ action('ReplayController@download', compact('replay')) }}">Download</a></td>
				<td><a href="srb2kartreplay://{{ $replay->getKey() }}">Watch</a></td>
			</tr>
		@endforeach
	</tbody>
</table>

{{ $replays->links() }}
@stop
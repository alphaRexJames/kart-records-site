@extends('layouts.admin')

@section('content')
<h1>Courses</h1>

<ul>
@foreach($courses as $course)
	<li><a href="{{ action('Admin\CourseController@show', compact('course')) }}">{{ $course->name }}</a></li>
@endforeach
</ul>

<a href="{{ action('Admin\CourseController@create') }}">Add New Course</a>
@stop

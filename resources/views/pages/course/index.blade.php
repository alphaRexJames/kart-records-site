@extends('layouts.front')

@section('content')
<h1>Courses</h1>

<ul>
@foreach($courses as $course)
	<li><a href="{{ action('CourseController@show', compact('course')) }}">{{ $course->name }}</a></li>
@endforeach
</ul>
@stop

<!doctype html>
<html>
	<head>
		<title>@yield('title', 'Kart Records')</title>
		<link href="/style.css" rel="stylesheet" />
	</head>

	<body>
		<div id="header">
			<div id="sitename">Kart Records</div>
			<nav>
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="{{ action('CourseController@index') }}">Courses</a></li>
					<li><a href="{{ action('ReplayController@create') }}">Upload Replay</a></li>
				</ul>
			</nav>
		</div>

		<div id="content">
			@yield('content')
		</div>
	</body>
</html>

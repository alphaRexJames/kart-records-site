<table>
	<thead>
		<tr>
			<th>Name</th>
			<th>Time</th>
			<th>Character</th>
			<th>Verification</th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach($leaderboard as $replay)
			<tr>
				<td>{{ $replay->name }}</td>
				<td>{{ $replay->$time }}</td>
				<td>{{ $replay->character->name ?: $replay->character->skin }}</td>
				<td>{{ $replay->status }}</td>
				<td><a href="{{ action('ReplayController@show', compact('replay')) }}">Info</a></td>
				<td><a href="{{ action('ReplayController@download', compact('replay')) }}">Download</a></td>
				<td><a href="srb2kartreplay://{{ $replay->getKey() }}">Watch</a></td>
			</tr>
		@endforeach
	</tbody>
</table>
